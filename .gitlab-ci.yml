stages:
  - prepare
  - build_and_test
  - deploy
  - release

# If you are looking for a place where to add 'UNITY_LICENSE_FILE' and other secrets, please visit your project's gitlab page:
# settings > CI/CD > Variables instead
variables:
  BUILD_NAME: 6dposeestimation-datasetprovider
  UNITY_ACTIVATION_FILE: ./unity3d.alf
  IMAGE: unityci/editor # https://hub.docker.com/r/unityci/editor
  IMAGE_VERSION: "0" # This will automatically use latest v0.x.x, see https://github.com/game-ci/docker/releases
  UNITY_DIR: $CI_PROJECT_DIR # this needs to be an absolute path. Defaults to the root of your tree.
  # You can expose this in Unity via Application.version
  VERSION_NUMBER_VAR: $CI_COMMIT_REF_SLUG-$CI_PIPELINE_ID-$CI_JOB_ID
  VERSION_BUILD_VAR: $CI_PIPELINE_IID

image: $IMAGE:$UNITY_VERSION-base-$IMAGE_VERSION

get-unity-version:
  image: alpine
  stage: prepare
  variables:
    GIT_DEPTH: 1
  script:
    - echo UNITY_VERSION=$(cat $UNITY_DIR/ProjectSettings/ProjectVersion.txt | grep "m_EditorVersion:.*" | awk '{ print $2}') | tee prepare.env
  artifacts:
    reports:
      dotenv: prepare.env

.unity_before_script: &unity_before_script
  before_script:
    - chmod +x ./ci/before_script.sh && ./ci/before_script.sh
  needs:
    - job: get-unity-version
      artifacts: true

.cache: &cache
  cache:
    key: "$CI_PROJECT_NAMESPACE-$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-$TEST_PLATFORM"
    paths:
      - $UNITY_DIR/Library/

.license: &license
  rules:
    - if: '$UNITY_LICENSE != null'
      when: always

.unity_defaults: &unity_defaults
  <<:
    - *unity_before_script
    - *cache
    - *license

# run this job when you need to request a license
# you may need to follow activation steps from documentation
get-activation-file:
  <<: *unity_before_script
  rules:
    - if: '$UNITY_LICENSE == null'
      when: manual
  stage: prepare
  script:
    - chmod +x ./ci/get_activation_file.sh && ./ci/get_activation_file.sh
  artifacts:
    paths:
      - $UNITY_ACTIVATION_FILE
    expire_in: 10 min # Expiring this as artifacts may contain sensitive data and should not be kept public

.test: &test
  stage: build_and_test
  <<: *unity_defaults
  script:
    - chmod +x ./ci/test.sh && ./ci/test.sh
  artifacts:
    when: always
    expire_in: 2 weeks
  # https://gitlab.com/gableroux/unity3d-gitlab-ci-example/-/issues/83
  coverage: /<Linecoverage>(.*?)</Linecoverage>/

# Tests without junit reporting results in GitLab
# test-playmode:
#   <<: *test
#   variables:
#     TEST_PLATFORM: playmode
#     TESTING_TYPE: NUNIT

# test-editmode:
#   <<: *test
#   variables:
#     TEST_PLATFORM: editmode
#     TESTING_TYPE: NUNIT

# uncomment the following blocks if you'd like to have junit reporting unity test results in gitlab
# We currently have the following issue which prevents it from working right now, but you can give
# a hand if you're interested in this feature:
# https://gitlab.com/gableroux/unity3d-gitlab-ci-example/-/issues/151

.test-with-junit-reports: &test-with-junit-reports
  stage: build_and_test
  <<: *unity_defaults
  script:
    # This could be made faster by adding these packages to base image or running in a separate job (and step)
    # We could use an image with these two depencencies only and only do the saxonb-xslt command on
    # previous job's artifacts
    - apt-get update && apt-get install -y default-jre libsaxonb-java
    - chmod +x ./ci/test.sh && ./ci/test.sh
    - saxonb-xslt -s $UNITY_DIR/$TEST_PLATFORM-results.xml -xsl $CI_PROJECT_DIR/ci/nunit-transforms/nunit3-junit.xslt >$UNITY_DIR/$TEST_PLATFORM-junit-results.xml
  artifacts:
    when: always
    paths:
    # This is exported to allow viewing the Coverage Report in detail if needed
    - $UNITY_DIR/$TEST_PLATFORM-coverage/
    reports:
      junit:
        - $UNITY_DIR/$TEST_PLATFORM-junit-results.xml
        - "$UNITY_DIR/$TEST_PLATFORM-coverage/coverage.xml"
    expire_in: 2 weeks
  # https://gitlab.com/gableroux/unity3d-gitlab-ci-example/-/issues/83
  coverage: /<Linecoverage>(.*?)</Linecoverage>/

test-playmode-with-junit-reports:
  <<: *test-with-junit-reports
  variables:
    TEST_PLATFORM: playmode
    TESTING_TYPE: JUNIT

test-editmode-with-junit-reports:
  <<: *test-with-junit-reports
  variables:
    TEST_PLATFORM: editmode
    TESTING_TYPE: JUNIT

.build: &build
  stage: build_and_test
  <<: *unity_defaults
  rules:
    - if: $CI_COMMIT_TAG
  artifacts:
    paths:
      - $UNITY_DIR/Builds/
    reports:
      dotenv: release.env
  # https://gitlab.com/gableroux/unity3d-gitlab-ci-example/-/issues/83

build-StandaloneLinux64:
  <<: *build
  script:
    - echo "LINUX=$CI_JOB_ID" >> release.env
    - chmod +x ./ci/build.sh && ./ci/build.sh
  variables:
    BUILD_TARGET: StandaloneLinux64

#build-StandaloneOSX:
#  <<: *build
#  image: $IMAGE:$UNITY_VERSION-mac-mono-$IMAGE_VERSION
#  script:
#    - echo "MACOS=$CI_JOB_ID" >> release.env
#    - chmod +x ./ci/build.sh && ./ci/build.sh
#  variables:
#    BUILD_TARGET: StandaloneOSX

#Note: build target names changed in recent versions, use this for versions < 2017.2:
# build-StandaloneOSXUniversal:
#   <<: *build
#   variables:
#     BUILD_TARGET: StandaloneOSXUniversal

build-StandaloneWindows64:
  <<: *build
  image: $IMAGE:$UNITY_VERSION-windows-mono-$IMAGE_VERSION
  script:
    - echo "WINDOWS=$CI_JOB_ID" >> release.env
    - chmod +x ./ci/build.sh && ./ci/build.sh
  variables:
    BUILD_TARGET: StandaloneWindows64

pages:
  image: alpine:latest
  stage: deploy
  script:
    - mv "$UNITY_DIR/Builds/WebGL/${BUILD_NAME}" public
  artifacts:
    paths:
      - public
  only:
    - main

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: always

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "running release_job"
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Created using the release-cli $EXTRA_DESCRIPTION'  # $EXTRA_DESCRIPTION must be defined
    tag_name: '$CI_COMMIT_TAG'                                       # elsewhere in the pipeline.
    ref: '$CI_COMMIT_TAG'
    assets: # Optional, multiple asset links
      links:
        - name: 'Windows'
          url: 'https://gitlab.gwdg.de/6dposeestimation/6dposeestimation-datasetprovider/-/jobs/${WINDOWS}/artifacts/download'
        - name: 'Linux'
          url: 'https://gitlab.gwdg.de/6dposeestimation/6dposeestimation-datasetprovider/-/jobs/${LINUX}/artifacts/download'
#        - name: 'MacOS'
#          url: 'https://gitlab.gwdg.de/6dposeestimation/6dposeestimation-datasetprovider/-/jobs/${MACOS}/artifacts/download'
