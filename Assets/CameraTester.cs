using System.Collections;
using System.Collections.Generic;
using ImageSynthesis;
using UnityEngine;

public class CameraTester : MonoBehaviour
{
    void Start()
    {
        var cam = GetComponent<Camera>();
        CameraFiltersScript cameraFiltersScript = cam.gameObject.GetComponent<CameraFiltersScript>();

        cameraFiltersScript.SetShaderEffect(ImageType.Segmentation);
        cameraFiltersScript.SetSegmentationEffect();
    }
}