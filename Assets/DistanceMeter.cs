using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceMeter : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject camera;

    // Update is called once per frame
    void Update()
    {
        var scanObjects = GameObject.FindGameObjectsWithTag("ScanObject");
        if (scanObjects.Length >= 1)
        {
            var collider = scanObjects[0].GetComponent<Renderer>();
            //Debug.Log(camera.transform.InverseTransformPoint(collider.bounds.center));
        }

        var transform1 = transform;
        Debug.Log(Quaternion.Inverse(Quaternion.LookRotation(-transform1.forward, transform1.up)) + " " + Quaternion.identity);
    }
}