using System;
using System.Collections;
using System.Collections.Generic;
using HandleData;
using UnityEngine;
using UnityEngine.UI;

public class ConnectAllKeypoints : MonoBehaviour
{
    public GameObject line;

    public Slider lineSizeSlider;

    public void ConnectKeypoints()
    {
        var allKeypoints = GameObject.FindGameObjectsWithTag("Keypoint");
        foreach (var keypoint in allKeypoints)
        {
            var keypointTransform = keypoint.transform;
            var closestPoints = new List<Tuple<float, Transform>>();
            foreach (Transform child in keypoint.transform.parent.transform)
            {
                if (child.CompareTag("Keypoint"))
                {
                    closestPoints.Add(
                        new Tuple<float, Transform>((keypointTransform.position - child.position).sqrMagnitude, child));
                }
            }

            closestPoints.Sort((a, b) => a.Item1.CompareTo(b.Item1));

            if (closestPoints.Count > 5)
            {
                closestPoints = closestPoints.GetRange(0, 5);
            }

            foreach (var point in closestPoints)
            {
                var child = point.Item2;
                if (!child.CompareTag("Keypoint") || keypointTransform == child || keypointTransform
                        .GetComponent<ConnectedKeypointsArray>().GETConnectedKeypoints()
                        .Contains(child.name) || child
                        .GetComponent<ConnectedKeypointsArray>().GETConnectedKeypoints()
                        .Contains(keypointTransform.name)) continue;
                child.GetComponent<ConnectedKeypointsArray>().AddKeypoint(keypointTransform.name);
                keypointTransform.GetComponent<ConnectedKeypointsArray>().AddKeypoint(child.name);
                var lineObject = Instantiate(line, keypointTransform.parent, true);
                var size = lineSizeSlider.value;
                lineObject.transform.localScale = new Vector3(size * 4, size * 4,
                    Vector3.Distance(keypointTransform.position, child.position) /
                    keypointTransform.parent.localScale.x);

                lineObject.transform.position = keypointTransform.position; // place bond here
                lineObject.transform.LookAt(child.position);
                lineObject.AddComponent<LineKeypoints>();
                var lineKeypoints = lineObject.GetComponent<LineKeypoints>();
                lineKeypoints.keypoint1 = keypointTransform;
                lineKeypoints.keypoint2 = child;
            }
        }
    }
}