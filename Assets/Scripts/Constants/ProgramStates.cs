﻿using System;

namespace Constants
{
    public enum ProgramStates
    {
        Keypointmode,
        Selectmode,
        Skeletonmode,
        Recordmode,
    }
}