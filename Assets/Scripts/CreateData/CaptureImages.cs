using System.Collections.Generic;
using System.Linq;
using HandleData;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;
using Utils;
using BoundingBox = Utils.BoundingBox;
using Button = UnityEngine.UI.Button;
using Random = System.Random;
using Toggle = UnityEngine.UI.Toggle;

namespace CreateData
{
    public class CaptureImages : MonoBehaviour
    {
        // Start is called before the first frame update
        public Button captureImageButton;
        public InputField amountInputField;
        public Canvas ui;
        public Toggle rayTracingToggle;
        public int samplesRaytracing = 128;
        private int _number = 0;
        public GameObject[] cameras;
        public Toggle[] toggleButtons;
        public Light directionalLight;
        public Volume globalVolume;
        public GameObject frame;
        public Cubemap[] hdriBackgroundImages;
        public bool randomizeAfterEachFrame = false;
        public SemanticSegmentation semanticSegmentation;

        private List<int> _activeCameras = new List<int>();
        private readonly Random _rnd = new Random();
        private GameObject[] _scanObjects;
        private List<GameObject> _duplicatedObjects = new List<GameObject>();
        private GameObject[] _keyPoints;
        private GameObject[] _lines;
        private Vector3 _oldLightRotation;
        private Dictionary<string, int> _colorMapping;
        private bool _rgbScreenshot = false;
        private int _camIndex = -1;

        void Start()
        {
            captureImageButton.onClick.AddListener(() => SetScreenshots(int.Parse(amountInputField.text)));
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                _number = 0;
            }

            if (_activeCameras.Count <= 0) return;
            if (_number > 0)
            {
                if (!_rgbScreenshot && _camIndex == -1)
                {
                    RandomizeScene();
                    _camIndex = 0;
                }

                var cam = _activeCameras[_camIndex];
                var activeCamera = cameras[cam].GetComponent<Camera>();
                ChangeView(cam);

                if (!_rgbScreenshot)
                {
                    _colorMapping =
                        semanticSegmentation.EnableSemanticSegmentation(_scanObjects.Concat(_duplicatedObjects)
                            .ToArray());
                    SaveKeypoints(activeCamera, cam);
                    SaveImage(activeCamera, cam, "segmentation");
                    _rgbScreenshot = !_rgbScreenshot;
                }
                else
                {
                    semanticSegmentation.DisableSemanticSegmentation(_scanObjects.Concat(_duplicatedObjects).ToArray());
                    for (int i = 0; i < 10; i++)
                    {
                        activeCamera.Render();
                    }

                    SaveImage(activeCamera, cam);

                    _rgbScreenshot = !_rgbScreenshot;
                }


                if (!_rgbScreenshot)
                {
                    _camIndex += 1;
                }

                if (!_rgbScreenshot && _camIndex == _activeCameras.Count)
                {
                    _camIndex = -1;
                    _number--;
                }
            }
            else if (_number == 0)
            {
                semanticSegmentation.DisableSemanticSegmentation(_scanObjects.Concat(_duplicatedObjects).ToArray());
                ResetScene();
                _number--;
            }
        }

        private void SaveImage(Camera targetCam, int camNumber, string info = "image")
        {
            ScreenCapture.CaptureScreenshot(System.IO.Directory.GetCurrentDirectory() + "/trainData/" + info +
                                            camNumber +
                                            "-" + _number + ".png");
        }

        private void SaveKeypoints(Camera activeCamera, int nextView)
        {
            var scanObjectsData = new List<ObjectKeyPoints>();

            foreach (var scanObject in _scanObjects.Concat(_duplicatedObjects))
            {
                var keypointPositions = new List<Vector3>();
                foreach (Transform transformChild in scanObject.transform)
                {
                    if (transformChild.CompareTag("Keypoint"))
                    {
                        Vector2 screenPosition = activeCamera.WorldToViewportPoint(transformChild.position);
                        if (screenPosition.x < 0 || screenPosition.x > 1 || screenPosition.y < 0 ||
                            screenPosition.y > 1)
                        {
                            keypointPositions.Add(new Vector3(screenPosition.x, screenPosition.y, 0));
                        }
                        else
                        {
                            var position = activeCamera.transform.position;
                            var hits = Physics.RaycastAll(position, (transformChild.position - position));
                            var keypointIndex =
                                hits.ToList().FindIndex(hit => hit.transform == transformChild.transform);
                            var closestDistance = hits.ToList().FindAll(hit => hit.transform.CompareTag("ScanObject"))
                                .Min(hit => hit.distance);

                            if (keypointIndex == -1)
                            {
                                //error
                                keypointPositions.Add(new Vector3(screenPosition.x, screenPosition.y, -1));
                            }
                            else
                            {
                                keypointPositions.Add(new Vector3(screenPosition.x, screenPosition.y,
                                    (hits[keypointIndex].distance < closestDistance) ? 1 : 0));
                            }
                        }
                    }
                }

                var trans = activeCamera.transform.InverseTransformPoint(
                    scanObject.GetComponent<Renderer>().bounds.center);
                trans[1] = -trans[1];
                trans = trans * 100;
                var objectKeyPoints = new ObjectKeyPoints()
                {
                    name = scanObject.name,
                    keyPointScreenCoordinates = keypointPositions.ToArray(),
                    rotation = Quaternion.Inverse(Quaternion.LookRotation(-activeCamera.transform.forward,
                                   activeCamera.transform.up)) *
                               scanObject.transform.rotation,
                    translation = trans,
                    bbox = BoundingBox.Camera2DBoundingBoxPrecise(scanObject, activeCamera),
                    color = _colorMapping[scanObject.name]
                };
                scanObjectsData.Add(objectKeyPoints);
            }

            var focalLength = activeCamera.focalLength;
            var cameraTransform = activeCamera.transform;
            var radVFOV = activeCamera.fieldOfView * Mathf.Deg2Rad;
            var radHFOV = 2 * Mathf.Atan(Mathf.Tan(radVFOV / 2) * activeCamera.aspect);
            var cameraInformation = new CameraInformation
            {
                translation = cameraTransform.position,
                rotation = cameraTransform.rotation,
                width = activeCamera.pixelWidth,
                height = activeCamera.pixelHeight,
                skew = 0.0F,
                cx = activeCamera.pixelWidth / 2F,
                cy = activeCamera.pixelHeight / 2F,
                fx = (activeCamera.pixelWidth / 2F) / Mathf.Tan(radHFOV / 2),
                fy = (activeCamera.pixelHeight / 2F) / Mathf.Tan(radVFOV / 2),
                depthScale = 0.1F
            };
            var data = new PhotoKeyPointLocationData
                { scanObjectsData = scanObjectsData.ToArray(), cameraInformation = cameraInformation };
            SaveData.SavePhotoKeyPointLocations(data, "data" + nextView + "-" + _number);
        }

        private void RandomizeScene()
        {
            foreach (var scanObject in _scanObjects.Concat(_duplicatedObjects))
            {
                var scanObjectSpace = scanObject.GetComponent<ObjectRandomizer>();
                scanObjectSpace.RandomizePositionAndOrientation();
            }

            directionalLight.transform.eulerAngles = new Vector3(_rnd.Next(360), _rnd.Next(360), _rnd.Next(360));

            if (globalVolume.profile.TryGet<HDRISky>(out HDRISky hdriSky))
            {
                hdriSky.hdriSky.value = hdriBackgroundImages[_rnd.Next(hdriBackgroundImages.Length)];
            }
        }

        private void ResetScene()
        {
            ui.enabled = true;
            frame.SetActive(true);
            foreach (var keyPoint in _keyPoints)
            {
                keyPoint.GetComponent<MeshRenderer>().enabled = true;
            }

            foreach (var line in _lines)
            {
                line.SetActive(true);
            }

            foreach (var scanObject in _scanObjects)
            {
                var scanObjectSpace = scanObject.GetComponent<ObjectRandomizer>();
                scanObjectSpace.ResetPosition();
            }

            foreach (var scanObject in _duplicatedObjects)
            {
                Destroy(scanObject);
            }

            _duplicatedObjects = new List<GameObject>();

            directionalLight.transform.eulerAngles = _oldLightRotation;
        }

        private void SetScreenshots(int number)
        {
            this._number = number;
            if (number > 0)
            {
                _scanObjects = GameObject.FindGameObjectsWithTag("ScanObject");

                ui.enabled = false;
                frame.SetActive(false);
                _keyPoints = GameObject.FindGameObjectsWithTag("Keypoint");
                foreach (var keyPoint in _keyPoints)
                {
                    keyPoint.GetComponent<MeshRenderer>().enabled = false;
                }

                _lines = GameObject.FindGameObjectsWithTag("Line");
                foreach (var line in _lines)
                {
                    line.SetActive(false);
                }

                foreach (var scanObject in _scanObjects)
                {
                    var scanObjectSpace = scanObject.GetComponent<ObjectRandomizer>();
                    Destroy(scanObject.GetComponent<CombineMeshes>());
                    for (int i = 0; i < scanObjectSpace.instances - 1; i++)
                    {
                        _duplicatedObjects.Add(Instantiate(scanObject, scanObject.transform.parent));
                        _duplicatedObjects.Last().name = scanObject.name + i;
                        _duplicatedObjects.Last().GetComponent<ObjectRandomizer>().RefreshRandom();
                    }
                }

                SaveObjectDataButton.OnClickSave();
            }

            _activeCameras = new List<int>();
            for (var i = 0; i < toggleButtons.Length; i++)
            {
                if (toggleButtons[i].isOn)
                {
                    _activeCameras.Add(i);
                }
            }
        }

        private void ChangeView(int number)
        {
            foreach (var cam in cameras)
            {
                //cam.SetActive(false);
                cam.GetComponent<Camera>().enabled = false;
                cam.tag = "Untagged";
            }

            //cameras[number].SetActive(true);
            cameras[number].GetComponent<Camera>().enabled = true;
            cameras[number].tag = "MainCamera";
        }

        public void SetRandomizeAfterEachFrame(bool active)
        {
            randomizeAfterEachFrame = active;
        }

        private static string RemoveFromEnd(string s, string suffix)
        {
            if (s.EndsWith(suffix))
            {
                return s.Substring(0, s.Length - suffix.Length);
            }

            return s;
        }
    }
}