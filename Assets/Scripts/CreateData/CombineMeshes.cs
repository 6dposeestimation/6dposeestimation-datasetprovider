using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CombineMeshes : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var position = transform.position;
        transform.position = Vector3.zero;
        var scale = transform.localScale;
        transform.localScale = Vector3.one;
        var rotation = transform.rotation;
        transform.rotation = Quaternion.identity;
        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        List<CombineInstance> combine = new List<CombineInstance>();
        MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
        List<Material> materials = new List<Material>();

        int i = 0;
        while (i < meshFilters.Length)
        {
            for (int j = 0; j < meshFilters[i].mesh.subMeshCount; j++)
            {
                var combineInstance = new CombineInstance();
                combineInstance.mesh = meshFilters[i].mesh;
                combineInstance.transform = meshFilters[i].transform.localToWorldMatrix;
                combineInstance.subMeshIndex = j;
                combine.Add(combineInstance);
                materials.Add(meshRenderers[i].materials[j]);
            }
            meshFilters[i].gameObject.SetActive(false);
            i++;
        }

        var mesh = new Mesh
        {
            indexFormat = UnityEngine.Rendering.IndexFormat.UInt32
        };
        mesh.CombineMeshes(combine.ToArray(),false);
        transform.GetComponent<MeshFilter>().mesh = mesh;
        transform.GetComponent<MeshCollider>().sharedMesh = mesh;
        GetComponent<MeshRenderer>().materials = materials.Count > 1 ? materials.Skip(1).ToArray() : materials.ToArray();
        transform.gameObject.SetActive(true);
        transform.position = position;
        transform.localScale = scale;
        transform.rotation = rotation;
    }
}