using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ConvexCornerKeypoints : MonoBehaviour
{
    public Transform keyPointObject;
    private GameObject[] scanObjects;
    private Button button;
    private System.Random rnd = new System.Random();
    public int maxKeypoints = 100;

    // Start is called before the first frame update
    private void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnClickListener);
    }

    private void OnClickListener()
    {
        scanObjects = GameObject.FindGameObjectsWithTag("ScanObject");
        foreach (var scanObject in scanObjects)
        {
            var mesh = scanObject.GetComponent<MeshFilter>().mesh;
            if (mesh.isReadable)
            {
                var vertices = mesh.vertices.Select(x => new Vertex(x)).ToList();
                var result = MIConvexHull.ConvexHull.Create(vertices);
                var convexVertices = result.Points.Select(x => x.ToVec()).ToArray();

                Matrix4x4 m = Matrix4x4.Rotate(scanObject.transform.rotation);
                int i = 0;
                while (i < convexVertices.Length)
                {
                    convexVertices[i] = m.MultiplyPoint3x4(convexVertices[i]);
                    i++;
                }

                List<Vector3> finalVertices;
                if (convexVertices.Length > maxKeypoints)
                {
                    var filterVertices = convexVertices.ToList();

                    finalVertices = new List<Vector3>();
                    for (int j = 0; j < maxKeypoints; j++)
                    {
                        var index = rnd.Next(filterVertices.Count);
                        finalVertices.Add(convexVertices[index]);
                        filterVertices.RemoveAt(index);
                    }
                }
                else
                {
                    finalVertices = convexVertices.ToList();
                }

                foreach (var point in finalVertices)
                {
                    point.Scale(scanObject.transform.localScale);
                    var spawnPosition = point + scanObject.transform.position;


                    var newKeypoint = Instantiate(keyPointObject, spawnPosition,
                        Quaternion.identity);
                    newKeypoint.transform.parent = scanObject.transform;
                    var children = scanObject.transform.Cast<Transform>();
                    var number = 0;
                    if (children.Count() != 0)
                    {
                        number = children.Max(transformChild =>
                        {
                            if (int.TryParse(transformChild.name
                                        .Split(new[] {"Keypoint"}, StringSplitOptions.None).Last(),
                                    out var keypointNumber))
                            {
                                return keypointNumber + 1;
                            }
                            else
                            {
                                return 0;
                            }
                        });
                    }

                    newKeypoint.name = scanObject.name + "Keypoint" + number;
                }
            }
            else
            {
                print("Mesh " + mesh.name + " is not in Read/Write Mode");
            }
        }
    }
}