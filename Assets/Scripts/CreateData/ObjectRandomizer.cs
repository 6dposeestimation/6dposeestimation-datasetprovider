using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class ObjectRandomizer : MonoBehaviour
{
    private Vector3 _initTranslation;

    private Vector3 _initRotation;

    public Vector3 translationSpace = Vector3.zero;

    public Vector3 rotationSpace = Vector3.zero;

    public int instances = 1;

    private Random _rnd = new Random((int)DateTimeOffset.Now.ToUnixTimeMilliseconds());

    // Start is called before the first frame update
    void Start()
    {
        _initTranslation = transform.position;
        _initRotation = transform.eulerAngles;
    }

    public void RefreshRandom()
    {
        _rnd = new Random(gameObject.name.GetHashCode() + (int)DateTimeOffset.Now.ToUnixTimeMilliseconds());
    }

    public void RandomizePositionAndOrientation()
    {
        if ( GetComponent< Renderer>().bounds.center.magnitude > 6)
        {
            ResetPosition();
        }

        transform.position += new Vector3(RandomDouble(translationSpace.x),
            RandomDouble(translationSpace.y), RandomDouble(translationSpace.z));
        transform.eulerAngles += new Vector3(RandomDouble(rotationSpace.x),
            RandomDouble(rotationSpace.y), RandomDouble(rotationSpace.z));
    }

    public void ResetPosition()
    {
        transform.position = _initTranslation;
        transform.eulerAngles = _initRotation;
    }

    private float RandomDouble(float range)
    {
        return (float)_rnd.NextDouble() * range - (range / 2);
    }
}