using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectedKeypointsArray : MonoBehaviour
{
    private readonly HashSet<string> _connectedKeypoints = new HashSet<string>();

    public void AddKeypoint(string keypoint)
    {
        _connectedKeypoints.Add(keypoint);
    }

    public bool RemoveKeypoint(string keypoint)
    {
        return _connectedKeypoints.Remove(keypoint);
    }

    public HashSet<string> GETConnectedKeypoints()
    {
        return _connectedKeypoints;
    }
}
