﻿using System;
using UnityEngine;

namespace HandleData
{
    public class LineKeypoints : MonoBehaviour
    {
        public Transform keypoint1;
        public Transform keypoint2;

        public LineKeypoints(Transform keypoint1, Transform keypoint2)
        {
            this.keypoint1 = keypoint1;
            this.keypoint2 = keypoint2;
        }

        private void Update()
        {
            if (keypoint1 == null || keypoint2 == null)
            {
                Destroy(gameObject);
            }
        }
    }
}