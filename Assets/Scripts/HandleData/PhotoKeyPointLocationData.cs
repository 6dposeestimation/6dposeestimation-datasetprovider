﻿using System;
using UnityEngine;
using Utils;

namespace HandleData
{
    [Serializable]
    public struct PhotoKeyPointLocationData
    {
        public ObjectKeyPoints[] scanObjectsData;
        public CameraInformation cameraInformation;
    }

    [Serializable]
    public struct ObjectKeyPoints
    {
        public string name;
        public Vector3[] keyPointScreenCoordinates;
        public Quaternion rotation;
        public Vector3 translation;
        public BoundingBox.BBox bbox;
        public int color;
    }
}