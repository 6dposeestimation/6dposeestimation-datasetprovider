﻿using System;
using System.IO;
using UnityEngine;
using UnityTemplateProjects.SaveData;

namespace HandleData
{
    public class SaveData
    {
        public static void SaveKeyPoint(ScanObjectData saveObject)
        {
            File.WriteAllText("objectData/" + saveObject.objectName + ".json", JsonUtility.ToJson(saveObject));
        }

        public static void SaveKeyPoints(ScanObjectData[] saveObjects)
        {
            foreach (var scanObject in saveObjects)
            {
                File.WriteAllText("objectData/" + scanObject.objectName + ".json", JsonUtility.ToJson(scanObject));
            }
        }

        public static void SavePhotoKeyPointLocations(PhotoKeyPointLocationData data, String name)
        {
            File.WriteAllText("trainData/" + name + ".json", JsonUtility.ToJson(data));
        }

        public static ScanObjectData LoadKeyPoints(String name)
        {
            String data = File.ReadAllText("objectData/" + name + ".json");
            return JsonUtility.FromJson<ScanObjectData>(data);
        }
    }
}