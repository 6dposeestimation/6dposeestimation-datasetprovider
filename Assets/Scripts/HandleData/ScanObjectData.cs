using System;
using UnityEngine;

namespace UnityTemplateProjects.SaveData
{
    [Serializable]
    public class ScanObjectData
    {
        public string objectName;
        public KeypointData[] keypoints;
        public Vector3 translationSpace;
        public Vector3 rotationSpace;
        public ConnectedKeypoints[] skeleton;

        public ScanObjectData(string name, KeypointData[] keypoints,ConnectedKeypoints[] skeleton, Vector3 translationSpace, Vector3 rotationSpace)
        {
            this.objectName = name;
            this.translationSpace = translationSpace;
            this.rotationSpace = rotationSpace;
            this.keypoints = keypoints;
            this.skeleton = skeleton;
        }
    }

    [Serializable]
    public class KeypointData
    {
        public string name;
        public Vector3 position;

        public KeypointData(string name, Vector3 position)
        {
            this.position = position;
            this.name = name;
        }
    }

    [Serializable]
    public class ConnectedKeypoints
    {
        public string keypoint1;
        public string keypoint2;

        public ConnectedKeypoints(string keypoint1, string keypoint2)
        {
            this.keypoint1 = keypoint1;
            this.keypoint2 = keypoint2;
        }
    }
}