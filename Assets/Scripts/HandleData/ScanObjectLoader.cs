using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace HandleData
{
    public class ScanObjectLoader : MonoBehaviour
    {
        public GameObject transformGizmo;

        private void Start()
        {
            LoadAllModels();
        }

        public async void LoadAllModels()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }

            var files = Directory.GetFiles(Path.GetFullPath("models"));
            foreach (var file in files)
            {
                if (file.Split('.').Last().Equals("gltf"))
                {
                    await LoadModel(file);
                }
            }
        }

        private async Task LoadModel(string filePath)
        {
            if (!File.Exists(filePath) || !filePath.Split('.')[1].Equals("gltf"))
            {
                Debug.LogError("Please set FilePath to a valid path.");
                return;
            }

            var objectName = filePath.Split('/', '\\').Last().Split('.')[0];
            var newGameObject = new GameObject(objectName)
            {
                tag = "ScanObject",
                transform =
                {
                    parent = transform
                }
            };
            var gltf = newGameObject.AddComponent<GLTFast.GltfAsset>();
            newGameObject.AddComponent<MeshCollider>();
            newGameObject.AddComponent<ObjectRandomizer>();
            newGameObject.AddComponent<MeshFilter>();
            newGameObject.AddComponent<MeshRenderer>();
            gltf.loadOnStartup = false;
            var success = await gltf.Load(filePath);
            if (success)
            {
                newGameObject.AddComponent<CombineMeshes>();
                transformGizmo.SetActive(false);
                Instantiate(transformGizmo, newGameObject.transform);
                var transformScaleScroll = newGameObject.AddComponent<TransformScaleScroll>();
                transformScaleScroll.enabled = false;
            }
        }
    }
}