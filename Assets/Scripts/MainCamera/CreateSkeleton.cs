using System.Linq;
using Constants;
using HandleData;
using UI;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.UI;
using UnityTemplateProjects.SaveData;

namespace MainCamera
{
    public class CreateSkeleton : MonoBehaviour
    {
        public Camera mainCamera;

        public ModeToggleButton modeToggleButton;

        public Material keypointMaterial;

        public Material skeletonMaterial;

        public GameObject line;

        public Slider lineSizeSlider;


        // Start is called before the first frame update
        private Transform _firstKeypoint = null;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetMouseButtonDown(0) && modeToggleButton.GetKeypointMode() == ProgramStates.Skeletonmode)
            {
                Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);


                if (Physics.Raycast(ray, out var hit))
                {
                    Transform objectHit = hit.transform;
                    if (objectHit.CompareTag("Keypoint"))
                    {
                        if (_firstKeypoint == null)
                        {
                            _firstKeypoint = objectHit.transform;
                            _firstKeypoint.GetComponent<MeshRenderer>().material = skeletonMaterial;
                        }
                        else
                        {
                            if (objectHit.parent == _firstKeypoint.parent && objectHit != _firstKeypoint && !objectHit
                                .GetComponent<ConnectedKeypointsArray>().GETConnectedKeypoints()
                                .Contains(_firstKeypoint.name) && !_firstKeypoint
                                .GetComponent<ConnectedKeypointsArray>().GETConnectedKeypoints()
                                .Contains(objectHit.name))
                            {
                                _firstKeypoint.GetComponent<ConnectedKeypointsArray>().AddKeypoint(objectHit.name);
                                objectHit.GetComponent<ConnectedKeypointsArray>().AddKeypoint(_firstKeypoint.name);
                                var lineObject = Instantiate(line, _firstKeypoint.parent, true);
                                var size = lineSizeSlider.value;
                                lineObject.transform.localScale = new Vector3(size * 4, size * 4,
                                    Vector3.Distance(_firstKeypoint.position, objectHit.position) /
                                    _firstKeypoint.parent.localScale.x);

                                lineObject.transform.position = _firstKeypoint.position; // place bond here
                                lineObject.transform.LookAt(objectHit.position);
                                lineObject.AddComponent<LineKeypoints>();
                                var lineKeypoints = lineObject.GetComponent<LineKeypoints>();
                                lineKeypoints.keypoint1 = _firstKeypoint;
                                lineKeypoints.keypoint2 = objectHit;
                                _firstKeypoint.GetComponent<MeshRenderer>().material = keypointMaterial;
                                _firstKeypoint = null;
                            }
                            else
                            {
                                _firstKeypoint.GetComponent<MeshRenderer>().material = keypointMaterial;
                                _firstKeypoint = null;
                            }
                        }
                    }
                    else
                    {
                        if (_firstKeypoint != null)
                        {
                            _firstKeypoint.GetComponent<MeshRenderer>().material = keypointMaterial;
                            _firstKeypoint = null;
                        }
                    }

                    if (objectHit.CompareTag("Line"))
                    {
                        var lineKeypoints = objectHit.GetComponent<LineKeypoints>();
                        lineKeypoints.keypoint1.GetComponent<ConnectedKeypointsArray>()
                            .RemoveKeypoint(lineKeypoints.keypoint2.name);
                        lineKeypoints.keypoint2.GetComponent<ConnectedKeypointsArray>()
                            .RemoveKeypoint(lineKeypoints.keypoint1.name);
                        Destroy(hit.collider.transform.gameObject);
                    }
                }
            }
        }
    }
}