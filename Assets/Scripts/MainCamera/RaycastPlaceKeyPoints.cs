using System;
using System.Linq;
using Constants;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace MainCamera
{
    public class RaycastPlaceKeyPoints : MonoBehaviour
    {
        public Camera mainCamera;
        public Transform keyPointObject;
        public ModeToggleButton modeToggleButton;
        public Slider keypointSizeSlider;

        void Update()
        {
            if (Input.GetMouseButtonDown(0) && modeToggleButton.GetKeypointMode() == ProgramStates.Keypointmode)
            {
                Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);


                if (Physics.Raycast(ray, out var hit))
                {
                    Transform objectHit = hit.transform;
                    if (objectHit.CompareTag("ScanObject"))
                    {
                        var newKeypoint = Instantiate(keyPointObject, hit.point, Quaternion.identity);
                        var children = objectHit.transform.Cast<Transform>();
                        var number = 0;
                        if (children.Count() != 0)
                        {
                            number = children.Max(transformChild =>
                            {
                                if (int.TryParse(transformChild.name
                                        .Split(new[] { "Keypoint" }, StringSplitOptions.None).Last(),
                                    out var keypointNumber))
                                {
                                    return keypointNumber + 1;
                                }
                                else
                                {
                                    return 0;
                                }
                            });
                        }


                        newKeypoint.name = objectHit.name + "Keypoint" + number;
                        newKeypoint.transform.parent = objectHit;
                        var size = keypointSizeSlider.value;
                        newKeypoint.transform.localScale = new Vector3(size, size, size);
                    }

                    if (objectHit.CompareTag("Keypoint"))
                    {
                        Destroy(hit.collider.gameObject);
                    }
                }
            }
        }
    }
}