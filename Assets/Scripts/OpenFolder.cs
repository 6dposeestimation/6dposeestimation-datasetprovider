using System;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class OpenFolder : MonoBehaviour
{
    private void Start()
    {
        System.IO.Directory.CreateDirectory("objectData");
        System.IO.Directory.CreateDirectory("trainData");
        System.IO.Directory.CreateDirectory("models");
    }

    public void OpenDataFolder()
    {
        OpenFolderMultiPlatform("trainData");
    }

    public void OpenSaveFolder()
    {
        OpenFolderMultiPlatform("objectData");
    }

    public void OpenObjectFolder()
    {
        OpenFolderMultiPlatform("models");
    }

    private void OpenFolderMultiPlatform(string path)
    {
        path = Path.GetFullPath(path);
#if UNITY_STANDALONE_OSX
        Process.Start("open", $"-R \"{path}\"");
#elif UNITY_STANDALONE_WIN
        Process.Start("explorer.exe", path);
#elif UNITY_STANDALONE_LINUX
        Process.Start("file://" + path);
#endif
    }
}