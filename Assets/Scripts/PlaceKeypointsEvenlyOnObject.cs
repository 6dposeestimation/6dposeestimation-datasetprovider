using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlaceKeypointsEvenlyOnObject : MonoBehaviour
{
    public Transform keyPointObject;

    public void PlaceKeypoints()
    {
        foreach (var scanObject in GameObject.FindGameObjectsWithTag("ScanObject"))
        {
            var meshCollider = scanObject.GetComponent<MeshCollider>();
            var failCounter = 0;
            var keypointList = new List<Transform>();
            while (failCounter < 10)
            {
                var colliderPoint = new Vector3(Random.Range(meshCollider.bounds.min.x, meshCollider.bounds.max.x),
                    Random.Range(meshCollider.bounds.min.y, meshCollider.bounds.max.y),
                    Random.Range(meshCollider.bounds.min.z, meshCollider.bounds.max.z));
                var directionVector = (colliderPoint - meshCollider.bounds.center).normalized;
                var maxDistance = (float) Math.Sqrt(meshCollider.bounds.size.x * meshCollider.bounds.size.x +
                                                    meshCollider.bounds.size.y * meshCollider.bounds.size.y +
                                                    meshCollider.bounds.size.z * meshCollider.bounds.size.z) / 2;

                directionVector.Scale(new Vector3(maxDistance, maxDistance, maxDistance));
                //Debug.DrawRay(meshCollider.bounds.center, directionVector, Color.black, 10);
                RaycastHit[] hits =
                    Physics.RaycastAll(meshCollider.bounds.center + directionVector, -directionVector);

                foreach (var hit in hits)
                {
                    Transform objectHit = hit.transform;
                    if (objectHit == scanObject.transform && objectHit.CompareTag("ScanObject"))
                    {
                        if (!keypointList.Exists(
                                keypoint => (Math.Abs((keypoint.position - hit.point).magnitude) < 0.2)))
                        {
                            var newKeypoint = Instantiate(keyPointObject, hit.point,
                                Quaternion.identity);
                            newKeypoint.transform.parent = scanObject.transform;
                            var children = scanObject.transform.Cast<Transform>();
                            var number = 0;
                            if (children.Count() != 0)
                            {
                                number = children.Max(transformChild =>
                                {
                                    if (int.TryParse(transformChild.name
                                                .Split(new[] {"Keypoint"}, StringSplitOptions.None).Last(),
                                            out var keypointNumber))
                                    {
                                        return keypointNumber + 1;
                                    }
                                    else
                                    {
                                        return 0;
                                    }
                                });
                            }

                            newKeypoint.name = scanObject.name + "Keypoint" + number;
                            failCounter = 0;
                            keypointList.Add(newKeypoint);
                        }
                        else
                        {
                            failCounter++;
                        }
                    }
                }
            }
        }
    }
}