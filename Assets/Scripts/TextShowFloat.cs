using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextShowFloat : MonoBehaviour
{
    private Text _textValue;

    public void Awake()
    {
        _textValue = GetComponent<Text>();
    }

    public void SetValue(float value)
    {
        if (_textValue != null)
        {
            _textValue.text = "" + value;
        }
    }
}