using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformGizmo : MonoBehaviour
{
    // Start is called before the first frame update
    public int axis;


    private Vector3 mouseOffset;
    private float mouseZCoord;
    private bool _isPulled = false;


    private void Update()
    {
        switch (_isPulled)
        {
            case true when !Input.GetMouseButton(0):
                _isPulled = false;
                return;
            case false:
                return;
        }
        var parent = transform.parent.parent;
        var position = parent.position;
        position[axis] = (GetWorldMousePos() + mouseOffset)[axis];
        parent.position = position;
    }

    private void OnMouseDown()
    {
        _isPulled = true;
        mouseZCoord = Camera.main.WorldToScreenPoint(transform.position).z;
        mouseOffset = transform.position - GetWorldMousePos();
    }

    private Vector3 GetWorldMousePos()
    {
        var mousePosition = Input.mousePosition;
        mousePosition.z = mouseZCoord;
        return Camera.main.ScreenToWorldPoint(mousePosition);
    }
}