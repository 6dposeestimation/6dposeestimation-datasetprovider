using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformScaleScroll : MonoBehaviour
{
    // Start is called before the first frame update
    private Transform _transformGizmo;

    void Start()
    {
        foreach (Transform child in transform)
        {
            if (!child.name.Contains("Gizmo")) continue;
            _transformGizmo = child;
            break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        var scrollData = Input.mouseScrollDelta;
        if (scrollData.y < 0)
        {
            transform.localScale *= 0.9F;
        }
        else if (scrollData.y > 0)
        {
            transform.localScale *= 1.1F;
        }

        var localScale = transform.localScale;
        var gizmoLocalScale = new Vector3(0.1F / localScale.x, 0.1F / localScale.y, 0.1F / localScale.z);
        var size = (Camera.main.transform.position - transform.position).magnitude / 10;
        _transformGizmo.localScale = size * gizmoLocalScale;
    }
}