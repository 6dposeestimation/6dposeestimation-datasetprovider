using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    public GameObject[] cameras;
    public Button[] buttons;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            AddListenerToButton(i);   
        }
    }
    
    void AddListenerToButton(int i)
    {
        buttons[i].onClick.AddListener(() => ChangeView(i));
    }
    private void ChangeView(int number)
    {
        foreach (var cam in cameras)
        {
            //cam.SetActive(false);
            cam.GetComponent<Camera>().enabled = false;
            cam.tag = "Untagged";
        }
        //cameras[number].SetActive(true);
        cameras[number].GetComponent<Camera>().enabled = true;
        cameras[number].tag = "MainCamera";
    }
}
