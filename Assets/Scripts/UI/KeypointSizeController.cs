using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class KeypointSizeController : MonoBehaviour
{
    // Start is called before the first frame update
    public Slider slider;

    void Start()
    {
        slider.onValueChanged.AddListener(size =>
        {
            var keyPoints = GameObject.FindGameObjectsWithTag("Keypoint");
            foreach (var keyPoint in keyPoints)
            {
                keyPoint.transform.localScale = size * Vector3.one;
            }

            var lines = GameObject.FindGameObjectsWithTag("Line");

            foreach (var line in lines)
            {
                var length = line.transform.localScale.z;
                line.transform.localScale = new Vector3(size * 4F, size * 4F, length);
                
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
    }
}