using System;
using System.Collections;
using System.Collections.Generic;
using HandleData;
using UnityEngine;
using UnityEngine.UI;
using UnityTemplateProjects.SaveData;

public class LoadObjectDataButton : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform keyPointObject;
    public GameObject lineObject;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClickLoad);
    }

    private void OnClickLoad()
    {
        var scanObjects = GameObject.FindGameObjectsWithTag("ScanObject");
        foreach (var scanObject in scanObjects)
        {
            try
            {
                ScanObjectData scanObjectData = SaveData.LoadKeyPoints(scanObject.name);
                var position = scanObject.transform.position;
                scanObject.transform.position = Vector3.zero;
                var scale = scanObject.transform.localScale;
                scanObject.transform.localScale = Vector3.one;
                var rotation = scanObject.transform.rotation;
                scanObject.transform.rotation = Quaternion.identity;
                var keypoints = new List<Transform>();
                foreach (var keypoint in scanObjectData.keypoints)
                {
                    var loadedKeypoint = Instantiate(keyPointObject, keypoint.position,
                        Quaternion.identity);
                    loadedKeypoint.name = keypoint.name;
                    loadedKeypoint.transform.parent = scanObject.transform;
                    var keypointScale = loadedKeypoint.localScale;
                    loadedKeypoint.localScale = new Vector3(keypointScale.x / scale.x,
                        keypointScale.y / scale.y,
                        keypointScale.z / scale.z);
                    keypoints.Add(loadedKeypoint);
                }

                foreach (var line in scanObjectData.skeleton)
                {
                    Transform keypoint1 = null;
                    Transform keypoint2 = null;
                    foreach (var keypoint in keypoints)
                    {
                        if (line.keypoint1 == keypoint.name)
                        {
                            keypoint1 = keypoint;
                        }

                        if (line.keypoint2 == keypoint.name)
                        {
                            keypoint2 = keypoint;
                        }
                    }

                    if (keypoint1 != null && keypoint2 != null)
                    {
                        keypoint1.GetComponent<ConnectedKeypointsArray>().AddKeypoint(keypoint2.name);
                        var lineInstance = Instantiate(this.lineObject, keypoint1.parent, true);
                        lineInstance.transform.localScale = new Vector3(1, 1,
                            Vector3.Distance(keypoint1.position, keypoint2.position) /
                            keypoint1.parent.localScale.x);

                        lineInstance.transform.position = keypoint1.position; // place bond here
                        lineInstance.transform.LookAt(keypoint2.position);
                        lineInstance.AddComponent<LineKeypoints>();
                        var lineKeypoints = lineInstance.GetComponent<LineKeypoints>();
                        lineKeypoints.keypoint1 = keypoint1;
                        lineKeypoints.keypoint2 = keypoint2;
                    }
                }

                scanObject.transform.position = position;
                scanObject.transform.localScale = scale;
                scanObject.transform.rotation = rotation;

                var scanObjectSpace = scanObject.GetComponent<ObjectRandomizer>();
                scanObjectSpace.rotationSpace = scanObjectData.rotationSpace;
                scanObjectSpace.translationSpace = scanObjectData.translationSpace;
            }
            catch (Exception e)
            {
                print("No data stored for " + scanObject.name + ". Error: " + e.ToString());
            }
        }
    }
}