﻿using System;
using Constants;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ModeToggleButton : MonoBehaviour
    {
        public Button modeToggleButton;
        public Text modeToggleText;
        
        private ProgramStates _keypointMode = ProgramStates.Selectmode;

        private void Start()
        {
            modeToggleButton.onClick.AddListener(ToggleMode);
        }

        private void ToggleMode()
        {
            _keypointMode = _keypointMode switch
            {
                ProgramStates.Keypointmode => ProgramStates.Skeletonmode,
                ProgramStates.Selectmode => ProgramStates.Keypointmode,
                ProgramStates.Skeletonmode => ProgramStates.Selectmode,
                _ => ProgramStates.Selectmode
            };
            modeToggleText.text = _keypointMode switch
            {
                ProgramStates.Keypointmode => "Keypoint Mode",
                ProgramStates.Selectmode => "Select Mode",
                ProgramStates.Skeletonmode => "Skeleton Mode",
                _ => ""
            };
        }

        public void SetMode(ProgramStates mode)
        {
            _keypointMode = mode;
            modeToggleText.text = _keypointMode switch
            {
                ProgramStates.Keypointmode => "Keypoint Mode",
                ProgramStates.Selectmode => "Select Mode",
                _ => ""
            };
        }

        public ProgramStates GetKeypointMode()
        {
            return _keypointMode;
        }
    }
}