using System.Collections;
using System.Collections.Generic;
using Constants;
using MainCamera;
using UI;
using UnityEngine;
using UnityEngine.UI;

public class ObjectRandomizerRaycastSelecter : MonoBehaviour
{
    public Camera mainCamera;

    public ModeToggleButton modeToggleButton;

    public RaycastPlaceKeyPoints raycastPlaceKeyPoints;

    public GameObject randomizerUiElement;

    public Text objectName;

    public InputField translationXAxis;
    public InputField translationYAxis;
    public InputField translationZAxis;

    public Slider rotationXAxis;
    public Slider rotationYAxis;
    public Slider rotationZAxis;

    public Slider instances;

    public Text rotationXAxisText;
    public Text rotationYAxisText;
    public Text rotationZAxisText;

    public Text instancesText;

    private bool _updateValues = true;

    private Transform _selectedObject = null;

    // Start is called before the first frame update
    void Start()
    {
        translationXAxis.onEndEdit.AddListener((s) => UpdateRandomTranslation());
        translationYAxis.onEndEdit.AddListener((s) => UpdateRandomTranslation());
        translationZAxis.onEndEdit.AddListener((s) => UpdateRandomTranslation());

        rotationXAxis.onValueChanged.AddListener(value => UpdateRandomRotation(value, rotationXAxisText));
        rotationYAxis.onValueChanged.AddListener(value => UpdateRandomRotation(value, rotationYAxisText));
        rotationZAxis.onValueChanged.AddListener(value => UpdateRandomRotation(value, rotationZAxisText));

        instances.onValueChanged.AddListener(value => UpdateInstances(value));
    }

    // Update is called once per frame
    void Update()
    {
        if (modeToggleButton.GetKeypointMode() != ProgramStates.Selectmode)
        {
            randomizerUiElement.SetActive(false);
            SetTransformActive(false);
            _selectedObject = null;
        }

        if (Input.GetMouseButtonDown(0) && modeToggleButton.GetKeypointMode() == ProgramStates.Selectmode)
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit))
            {
                Transform objectHit = hit.transform;
                if (objectHit.CompareTag("ScanObject"))
                {
                    SetTransformActive(false);
                    var firstSelect = _selectedObject == null;
                    randomizerUiElement.SetActive(true);
                    objectName.text = objectHit.name;
                    _selectedObject = objectHit;

                    SetTransformActive(true);


                    var selectedObject = _selectedObject.GetComponent<ObjectRandomizer>();
                    _updateValues = false;
                    rotationXAxisText.text = selectedObject.rotationSpace.x + "°";
                    rotationYAxisText.text = selectedObject.rotationSpace.y + "°";
                    rotationZAxisText.text = selectedObject.rotationSpace.z + "°";

                    rotationXAxis.value = selectedObject.rotationSpace.x;
                    rotationYAxis.value = selectedObject.rotationSpace.y;
                    rotationZAxis.value = selectedObject.rotationSpace.z;

                    instances.value = selectedObject.instances;

                    translationXAxis.text = "" + selectedObject.translationSpace.x;
                    translationYAxis.text = "" + selectedObject.translationSpace.y;
                    translationZAxis.text = "" + selectedObject.translationSpace.z;

                    instancesText.text = "" + selectedObject.instances;
                    _updateValues = true;
                }
            }
        }
    }

    void UpdateRandomRotation(float value, Text slider)
    {
        if (_selectedObject != null && _updateValues)
        {
            var selectedObject = _selectedObject.GetComponent<ObjectRandomizer>();
            selectedObject.rotationSpace = new Vector3(rotationXAxis.value, rotationYAxis.value, rotationZAxis.value);
            slider.text = (int)value + "°";
        }
    }

    void UpdateInstances(float value)
    {
        if (_selectedObject != null && _updateValues)
        {
            var selectedObject = _selectedObject.GetComponent<ObjectRandomizer>();
            selectedObject.instances = (int)value;
        }
    }

    void UpdateRandomTranslation()
    {
        if (_selectedObject != null && _updateValues)
        {
            var selectedObject = _selectedObject.GetComponent<ObjectRandomizer>();
            selectedObject.translationSpace = new Vector3(Mathf.Abs(int.Parse(translationXAxis.text)),
                Mathf.Abs(int.Parse(translationYAxis.text)), Mathf.Abs(int.Parse(translationZAxis.text)));
        }
    }

    public void SetTransformActive(bool active)
    {
        if (_selectedObject != null)
        {
            foreach (Transform child in _selectedObject.transform)
            {
                if (!child.name.Contains("Gizmo")) continue;
                child.gameObject.SetActive(active);
                break;
            }

            _selectedObject.GetComponent<TransformScaleScroll>().enabled = active;
        }
    }

    public void ResetSelection()
    {
        SetTransformActive(false);
        _selectedObject = null;
        randomizerUiElement.SetActive(false);
    }
}