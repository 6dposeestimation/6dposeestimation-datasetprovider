using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class RayTracingToggle : MonoBehaviour
{
    #region Renderpipelines

    public RenderPipelineAsset raytracingRenderPipelineAsset;
    public RenderPipelineAsset noRaytracingRenderPipelineAsset;

    #endregion

    #region Global Volume

    public Volume globalVolume;

    #endregion

    #region Volume Profiles

    public VolumeProfile raytracingVolumeProfile;
    public VolumeProfile noRaytracingVolumeProfile;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Toggle>().onValueChanged.AddListener(arg0 => EnableRaytracing(arg0));
    }

    // Update is called once per frame
    void EnableRaytracing(bool enable)
    {
        GraphicsSettings.renderPipelineAsset = enable ? raytracingRenderPipelineAsset : noRaytracingRenderPipelineAsset;
        globalVolume.profile = enable ? raytracingVolumeProfile : noRaytracingVolumeProfile;
    }
}