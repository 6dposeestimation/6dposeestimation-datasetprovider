using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemoveKeyPointsButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClickRemove);
    }

    private void OnClickRemove()
    {
        foreach (var keyPoint in GameObject.FindGameObjectsWithTag("Keypoint"))
        {
            Destroy(keyPoint);
        }
    }
}