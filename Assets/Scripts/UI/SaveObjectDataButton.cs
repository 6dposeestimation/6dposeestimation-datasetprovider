using System.Collections;
using System.Collections.Generic;
using HandleData;
using UnityEngine;
using UnityEngine.UI;
using UnityTemplateProjects.SaveData;

public class SaveObjectDataButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClickSave);
    }

    public static void OnClickSave()
    {
        var scanObjects = GameObject.FindGameObjectsWithTag("ScanObject");
        foreach (var scanObject in scanObjects)
        {
            var position = scanObject.transform.position;
            scanObject.transform.position = Vector3.zero;
            var scale = scanObject.transform.localScale;
            scanObject.transform.localScale = Vector3.one;
            var rotation = scanObject.transform.rotation;
            scanObject.transform.rotation = Quaternion.identity;
            var keypointPositions = new List<KeypointData>();
            var connectedKeypoints = new List<ConnectedKeypoints>();
            foreach (Transform transform in scanObject.transform)
            {
                if (transform.CompareTag("Keypoint"))
                {
                    keypointPositions.Add(new KeypointData(transform.name, transform.position));
                }

                if (transform.CompareTag("Line"))
                {
                    var lineKeypoints = transform.GetComponent<LineKeypoints>();
                    connectedKeypoints.Add(new ConnectedKeypoints(lineKeypoints.keypoint1.name,
                        lineKeypoints.keypoint2.name));
                }
            }

            var scanObjectSpace = scanObject.GetComponent<ObjectRandomizer>();
            SaveData.SaveKeyPoint(new ScanObjectData(scanObject.name, keypointPositions.ToArray(), connectedKeypoints.ToArray(),
                scanObjectSpace.translationSpace, scanObjectSpace.rotationSpace));
            scanObject.transform.position = position;
            scanObject.transform.localScale = scale;
            scanObject.transform.rotation = rotation;
        }
    }
}