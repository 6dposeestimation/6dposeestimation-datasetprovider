// inspired by: https://stackoverflow.com/questions/51905936/unity-function-to-access-the-2d-box-immediately-from-the-3d-pipeline

using System;
using UnityEngine;

namespace Utils
{
    public class BoundingBox
    {
        [Serializable]
        public struct BBox
        {
            public float x;
            public float y;
            public float width;
            public float height;
        }

        public static BBox Camera2DBoundingBox(GameObject go, Camera cam)
        {
            Vector3 cen = go.GetComponent<Renderer>().bounds.center;
            Vector3 ext = go.GetComponent<Renderer>().bounds.extents;
            Vector2[] extentPoints = new Vector2[8]
            {
                WorldToGUIPoint(new Vector3(cen.x - ext.x, cen.y - ext.y, cen.z - ext.z), cam),
                WorldToGUIPoint(new Vector3(cen.x + ext.x, cen.y - ext.y, cen.z - ext.z), cam),
                WorldToGUIPoint(new Vector3(cen.x - ext.x, cen.y - ext.y, cen.z + ext.z), cam),
                WorldToGUIPoint(new Vector3(cen.x + ext.x, cen.y - ext.y, cen.z + ext.z), cam),
                WorldToGUIPoint(new Vector3(cen.x - ext.x, cen.y + ext.y, cen.z - ext.z), cam),
                WorldToGUIPoint(new Vector3(cen.x + ext.x, cen.y + ext.y, cen.z - ext.z), cam),
                WorldToGUIPoint(new Vector3(cen.x - ext.x, cen.y + ext.y, cen.z + ext.z), cam),
                WorldToGUIPoint(new Vector3(cen.x + ext.x, cen.y + ext.y, cen.z + ext.z), cam)
            };
            Vector2 min = extentPoints[0];
            Vector2 max = extentPoints[0];
            foreach (Vector2 v in extentPoints)
            {
                min = Vector2.Min(min, v);
                max = Vector2.Max(max, v);
            }

            return new BBox { x = min.x, y = min.y, width = max.x - min.x, height = max.y - min.y };
        }

        public static BBox Camera2DBoundingBoxPrecise(GameObject go, Camera cam)
        {
            Vector3[] vertices = go.GetComponent<MeshFilter>().mesh.vertices;

            float x1 = float.MaxValue, y1 = float.MaxValue, x2 = 0.0f, y2 = 0.0f;

            foreach (Vector3 vert in vertices)
            {
                Vector2 tmp = WorldToGUIPoint(go.transform.TransformPoint(vert), cam);

                if (tmp.x < x1) x1 = tmp.x;
                if (tmp.x > x2) x2 = tmp.x;
                if (tmp.y < y1) y1 = tmp.y;
                if (tmp.y > y2) y2 = tmp.y;
            }

            return new BBox { x = x1, y = y1, width = x2 - x1, height = y2 - y1 };
        }

        public static Vector2 WorldToGUIPoint(Vector3 world, Camera cam)
        {
            Vector2 screenPoint = cam.WorldToScreenPoint(world);
            screenPoint.y = (float)Screen.height - screenPoint.y;
            return screenPoint;
        }
    }
}