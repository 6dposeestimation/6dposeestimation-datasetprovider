using System;
using UnityEngine;

namespace Utils
{
    [Serializable]
    public struct CameraInformation
    {
        public Vector3 translation;
        public Quaternion rotation;
        public float width;
        public float height;
        public float cx;
        public float cy;
        public float skew;
        public float fx;
        public float fy;
        public float depthScale;
    }
}