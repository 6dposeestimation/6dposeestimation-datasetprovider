using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public class SemanticSegmentation : MonoBehaviour
{
    public Shader defaultShader;

    public Volume defaultVolume;

    public Cubemap black;

    private Texture tmpCubemap;

    private bool enabled = false;

    private List<Material[]> materials;


    public Dictionary<string, int> EnableSemanticSegmentation(GameObject[] scanObjects)
    {
        SetSegmentationEffect(true);
        materials = new List<Material[]>();
        var colorDict = new Dictionary<string, int>();
        for (int i = 0; i < scanObjects.Length; i++)
        {
            var renderer = scanObjects[i].GetComponent<Renderer>();
            var mat = new Material(defaultShader);
            var colorVal = ((float)i / scanObjects.Length) / 2F + 0.5F;
            mat.color = new Color(colorVal, colorVal, colorVal);
            materials.Add(renderer.materials);
            renderer.materials = new[] { mat };
            colorDict.Add(scanObjects[i].name, (int)Math.Ceiling(colorVal * 255));
        }

        return colorDict;
    }

    public void DisableSemanticSegmentation(GameObject[] scanObjects)
    {
        SetSegmentationEffect(false);

        for (int i = 0; i < scanObjects.Length; i++)
        {
            scanObjects[i].GetComponent<Renderer>().materials = materials[i];
        }
    }

    private void SetSegmentationEffect(bool active)
    {
        if (defaultVolume.profile.TryGet<HDRISky>(out var hdriSky))
        {
            if (active)
            {
                tmpCubemap = hdriSky.hdriSky.value;
                hdriSky.hdriSky.value = black;
            }
            else
            {
                hdriSky.hdriSky.value = tmpCubemap;
            }
        }

        if (defaultVolume.profile.TryGet<Bloom>(out Bloom bloom))
        {
            bloom.active = !active;
        }

        if (defaultVolume.profile.TryGet<Tonemapping>(out Tonemapping tonemapping))
        {
            tonemapping.active = !active;
        }
    }
}