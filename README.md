# 6DPoseEstimation-DatasetProvider

## Usage

No installation needed. Just download it and start the program.

<a href = "https://gitlab.gwdg.de/6dposeestimation/6dposeestimation-datasetprovider/-/releases">Download</a>

## Unity

It is a Unity project and therfore it can simply be imported into Unity.

## TODO

- [ ] Names for keypoints

- [ ] Connect keypoints

- [ ] Random noise/dents for object texture and geometry 
